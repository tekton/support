<?php namespace Tekton\Support\Contracts;

interface AliasTranslation {
    function translate_alias($key);
}
