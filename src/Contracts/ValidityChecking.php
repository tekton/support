<?php namespace Tekton\Support\Contracts;

interface ValidityChecking {
    function is_valid();
}
