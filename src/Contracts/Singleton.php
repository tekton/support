<?php namespace Tekton\Support\Contracts;

interface Singleton {

    function instance();
}
