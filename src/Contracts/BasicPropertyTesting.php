<?php namespace Tekton\Support\Contracts;

interface BasicPropertyTesting {
    function is($property);
    function has($property);
}
