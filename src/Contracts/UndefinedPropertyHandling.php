<?php namespace Tekton\Support\Contracts;

interface UndefinedPropertyHandling {
    function get_property($key);
}
